const AppLocal = require('./App').appLocal;

AppLocal.listen(3000, () => {

    console.log(`App is running at http://localhost:3000`);
    console.info("Press CTRL-C to stop \n");
    process.on("unhandledRejection", error => {
        // Will print "unhandledRejection err is not defined"
        console.log("unhandledRejection", error.message);
    });
});