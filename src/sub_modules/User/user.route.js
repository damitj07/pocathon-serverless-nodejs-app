"use strict";
const express = require("express");
const controllers = require("./controllers");

const User = express.Router();
User.post("/signup", controllers.signupController)
    .post("/login", controllers.loginController)
    .get("/logout", controllers.logoutController)
    .get("/:userId", controllers.getUserDetailsController)
    .put("/:userId/updateProfile", controllers.updateUserProfileController)
    .get("/:userId/wishList", controllers.getUserWishListController)
    .put("/:userId/wishlist", controllers.addToWishlistController)
    .delete("/:userid/wishlist", controllers.removeFromWishlistController);

module.exports = User;