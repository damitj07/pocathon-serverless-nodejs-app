"use strict";

const responseHandler = require("./../../../utils/responseHandler");
const MongoDb = require("./../../../configure/db.configure").MongoDb;
const ObjectID = require("mongodb").ObjectID;

module.exports = (req, res) => {
    if (req.params.userId && req.body.urn)
        addToWishList(req.params.userId, req.body.urn)
            .then((result) => {
                res.send(responseHandler.responseSuccess(200, "School Added To WishList", result));
            })
            .catch((error) => {
                res.send(responseHandler.responseFail(500, error));
            });
    else
        res.send(responseHandler.responseFail(500, "Invalid Parameters"));
};
const addToWishList = (userId, urn) => {
    return new Promise((resolve, reject) => {
        MongoDb.users.findOne({ _id: new ObjectID(userId), wishlist: { $in: [+urn] } })
            .then((result) => {
                if (result) {
                    reject("Already Present In Wishlist");
                } else {
                    MongoDb.users.update({ "_id": new ObjectID(userId) }, { $push: { wishlist: +urn } }, (error, result) => {
                        if (error) reject(error);
                        resolve(result);
                    });
                }
            });
    });
};