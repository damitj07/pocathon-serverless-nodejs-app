"use strict";

const responseHandler = require("./../../../utils/responseHandler");

module.exports = (req, res) => {
    res.send(responseHandler.responseSuccess(200, "Logout successful", undefined));
};
