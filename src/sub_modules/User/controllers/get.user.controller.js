"use strict";
const responseHandler = require("./../../../utils/responseHandler");
const MongoDb = require("./../../../configure/db.configure").MongoDb;
const ObjectID = require("mongodb").ObjectID;

module.exports = (req, res) => {
    if (req.params.userId) {
        MongoDb.users.findOne(
            { _id: new ObjectID(req.params.userId) },
            { projection: { password: 0 } })
            .then((result) => {
                res.send(responseHandler.responseSuccess(200, "User details fetched !.", result));
            })
            .catch((error) => {
                res.send(responseHandler.responseFail(500, "Mongo query Failed"));
            });
    } else {
        res.send(responseHandler.responseFail(500, "Invalid Parameters"));
    }
};
