"use strict";

const dbConnectors = require("./../../../dbConnectors/mongoConnectors");
const responseHandler = require("./../../../utils/responseHandler");
const MongoDb = require("./../../../configure/db.configure").MongoDb;
const ObjectID = require("mongodb").ObjectID;

module.exports = (req, res) => {
    MongoDb.users.findOneAndUpdate(
        { "_id": new ObjectID(req.params.userId) },
        { $set: req.body },
        { projection: { password: 0 } }
    )
        .then((result) => {
            res.send(responseHandler.responseSuccess(200, "Profile updated successfully", result));
        }).catch(err => {
            res.send(responseHandler.responseFail(500, "Profile updated failed"));
        });
};