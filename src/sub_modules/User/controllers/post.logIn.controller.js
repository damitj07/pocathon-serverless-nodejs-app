"use strict";

const bcrypt = require("bcryptjs");
const jwt = require("jsonwebtoken");
const dbConnectors = require("./../../../dbConnectors/mongoConnectors");
const responseHandler = require("./../../../utils/responseHandler");
const MongoDb = require("./../../../configure/db.configure").MongoDb;
const ObjectID = require("mongodb").ObjectID;
const JWT_SECRET_KEY = process.env.JWT_SECRET_KEY || 'config.auth.secret'

module.exports = (req, res) => {
    dbConnectors.fetch(MongoDb.users, { email: req.body.email })
        .then((user) => {
            if (!user || !user.email) {
                res.send(responseHandler.responseFail(204, "Authentication failed. User Not Found."));
            }
            else if (!bcrypt.compareSync(req.body.password, user.password)) {
                res.send(responseHandler.responseFail(204, "Authentication failed. Wrong password."));
            }
            else {
                // if user is found and password is right, create a token
                const expiresIn = 86400;
                const token = jwt.sign({ email: user.email }, JWT_SECRET_KEY, {
                    expiresIn: expiresIn // expires in 24 hours
                });
                // TODO
                delete user.password;
                res.send(responseHandler.responseSuccess(200, "Authentication successful", {
                    expiresIn: expiresIn,
                    token: token,
                    user: user
                }));
            }
        })
        .catch((error) => {
            res.send(responseHandler.responseFail(500, JSON.stringify(error)));
        });
};
