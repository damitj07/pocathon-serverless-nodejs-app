"use strict";
const responseHandler = require("./../../../utils/responseHandler");
const MongoDb = require("./../../../configure/db.configure").MongoDb;
const ObjectID = require("mongodb").ObjectID;

module.exports = (req, res) => {
    if (req.params.userid && req.query.urn) {
        removeFromWishList(req.params.userid, req.query.urn)
            .then((result) => {
                res.send(responseHandler.responseSuccess(200, "School Removed From WishList", result));
            })
            .catch((error) => {
                res.send(error);
            });
    }
    else {
        res.send(responseHandler.responseFail(500, "Invalid Parameters"));
    }
};
const removeFromWishList = (userId, urn) => {
    return MongoDb.users.update(
        {
            $and: [{ "_id": new ObjectID(userId) }, { "wishlist": { "$in": [parseInt(urn)] } }]
        },
        {
            $pull: { "wishlist": { "$in": [parseInt(urn)] } }
        });
};
