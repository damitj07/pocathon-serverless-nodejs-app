"use strict";
const dbConnectors = require("./../../../dbConnectors/mongoConnectors");
const responseHandler = require("./../../../utils/responseHandler");
const MongoDb = require("./../../../configure/db.configure").MongoDb;
const ObjectID = require("mongodb").ObjectID;

module.exports = (req, res) => {
    if (req.params.userId) {
        const term = {
            query: {
                _id: new ObjectID(req.params.userId)
            },
            projection: {
                id: 1,
                wishlist: 1
            }
        };
        dbConnectors.fetchByQuery(MongoDb.users, term)
            .then((result) => {
                res.send(responseHandler.responseSuccess(200, "School wishList fetched successfully", result));
            })
            .catch((error) => {
                res.send(responseHandler.responseFail(500, "Mongo query Failed"));
            });
    }
    else {
        res.send(responseHandler.responseFail(500, "Invalid Parameters"));
    }
};