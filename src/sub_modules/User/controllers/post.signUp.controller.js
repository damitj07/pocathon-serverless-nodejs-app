"use strict";
const bcrypt = require("bcryptjs");
const jwt = require("jsonwebtoken");

const dbConnectors = require("./../../../dbConnectors/mongoConnectors");
const responseHandler = require("./../../../utils/responseHandler");
const MongoDb = require("./../../../configure/db.configure").MongoDb;
const JWT_SECRET_KEY = process.env.JWT_SECRET_KEY || 'config.auth.secret'

module.exports = (req, res) => {
    checkIfAlreadyExist(req.body.email)
        .then((isExist) => {
            if (isExist) {
                // res.send(responseHandler.responseFail(500, "User Already Exist."));
                return Promise.reject("User Already Exist.")
            } else {
                const userObj = {
                    firstName: req.body.firstName,
                    lastName: req.body.lastName,
                    dob: req.body.dob,
                    email: req.body.email,
                    phone1: req.body.phone1,
                    addressLine1: req.body.addressLine1,
                    addressLine2: req.body.addressLine2,
                    addressLine3: req.body.addressLine3,
                    addressLine4: req.body.addressLine4,
                    addressLine5: req.body.addressLine5,
                    password: bcrypt.hashSync(req.body.password, 10),
                    wishlist: []
                };
                return registerNewUser(userObj);
            }
        }).then((data) => {
            let insertedUser = data.ops[0];
            const expiresIn = 86400;
            const token = jwt.sign({ email: insertedUser.email }, JWT_SECRET_KEY, {
                expiresIn: expiresIn // expires in 24 hours
            });
            // TODO
            delete insertedUser.password;
            res.send(responseHandler.responseSuccess(200, "Authentication successful", {
                expiresIn: expiresIn,
                token: token,
                user: insertedUser
            }));
        }).catch((err) => {
            res.send(responseHandler.responseFail(500, JSON.stringify(err)));
        });
};
const registerNewUser = (userObj) => {
    return dbConnectors.insert(MongoDb.users, userObj);
};
const checkIfAlreadyExist = (email) => {
    return new Promise((resolve, reject) => {
        dbConnectors.fetch(MongoDb.users, { email: email })
            .then((user) => {
                user ? resolve(true) : resolve(false);
            }).catch(error => {
                reject(error);
            });
    });
};