
module.exports.signupController = require("./post.signUp.controller");
module.exports.logoutController = require("./get.logOut");
module.exports.loginController = require("./post.logIn.controller");
module.exports.addToWishlistController = require("./put.wishList.controller");
module.exports.getUserDetailsController = require("./get.user.controller");
module.exports.getUserWishListController = require("./get.wishList.controller");
module.exports.removeFromWishlistController = require("./delete.wishList.controller");
module.exports.updateUserProfileController = require("./put.updateUser.controller");