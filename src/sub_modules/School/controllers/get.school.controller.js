"use strict";

const dbConnectors = require("./../../../dbConnectors/mongoConnectors");
const responseHandler = require("./../../../utils/responseHandler");
const MongoDb = require("./../../../configure/db.configure").MongoDb;



module.exports = (req, res) => {
    const term = {
        query: { "uniqueReferenceNumber": parseInt(req.params.urn) }
    };
    dbConnectors.fetchByQuery(MongoDb.edubaseSchool, term)
        .then((school) => {
            res.send(responseHandler.responseSuccess(200, "School fetched successful", school));
        })
        .catch((error) => {
            res.send(responseHandler.responseFail(500, JSON.stringify(error)));
        });
};
