"use strict";

const dbConnectors = require("./../../../dbConnectors/mongoConnectors");
const responseHandler = require("./../../../utils/responseHandler");
const MongoDb = require("./../../../configure/db.configure").MongoDb;


module.exports = (req, res) => {
    const term = {
        query: { "URN": parseInt(req.params.urn) },
        projection: { "BELIG": 1, "GELIG": 1, "PTRWM_EXP": 1, "PTRWM_HIGH": 1, "MATPROG": 1, "WRITPROG": 1, "READPROG": 1 }
    };
    dbConnectors.fetchByQuery(MongoDb.statistics, term)
        .then((statistics) => {
            res.send(responseHandler.responseSuccess(200, "Statistics fetched successful", statistics));
        })
        .catch((error) => {
            res.send(responseHandler.responseFail(500, JSON.stringify(error)));
        });
};
