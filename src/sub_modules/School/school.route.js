"use strict";

const express = require("express");
const controllers = require("./controllers");

const School = express.Router();
School.get("/:urn", controllers.getSchoolDetailsController);
School.get("/:urn/statistics", controllers.getSchoolStatisticsController);

module.exports = School;