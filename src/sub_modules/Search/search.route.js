"use strict";

const express = require("express");
const controllers = require("./controllers");

const Search = express.Router();
Search
    .get("/", controllers.searchController);

module.exports = Search;