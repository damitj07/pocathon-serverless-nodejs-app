"use strict";

const dbConnectors = require("./../../../dbConnectors/mongoConnectors");
const responseHandler = require("./../../../utils/responseHandler");
const MongoDb = require("./../../../configure/db.configure").MongoDb;

module.exports = (req, res) => {
    const searchOptions = req.query;
    fullSearch(searchOptions)
        .then((searchResult) => {
        res.send(responseHandler.responseSuccess(200, "School search successful", searchResult));
    }).catch((error) => {
        res.send(responseHandler.responseFail(500, JSON.stringify(error)));
    });
};


const fullSearch = (searchOptions) => {
    if (searchOptions.searchTerm) {
        return searchOptions.searchBy === "postcode"
            ? searchByPostCode(searchOptions.searchTerm, projectionFieldForSchoolSearch)
            : searchBySchoolName(searchOptions.searchTerm, projectionFieldForSchoolSearch);
    }
    else {
        return advSearch(searchOptions);
    }
};

const searchBySchoolName = (searchTerm, projectionField) => {
    const regex = new RegExp(searchTerm, "i");
    const query = {
        $or: [
            { "label": regex },
            { "establishmentName": regex }
        ]
    };
    return new Promise((res, rej) => {
        MongoDb.edubaseSchool.find(query, projectionField)
            .limit(10)
            .toArray((error, documents) => {
            if (error)
                rej(error);
            res(documents);
        });
    });
};
const searchByPostCode = (searchTerm, projectionField) => {
    const regex = new RegExp(searchTerm, "i");
    return new Promise((res, rej) => {
        MongoDb.edubaseSchool.find({ "postcode": regex }, projectionField)
            .limit(10)
            .toArray((error, documents) => {
            if (error)
                rej(error);
            res(documents);
        });
    });
};

const advSearch = (searchOptions) => {
    return new Promise((resolve, reject) => {
        fetchSchool(searchOptions)
            .then((schoolData) => {
            const query = [];
            if (schoolData && searchOptions.distance) {
                const coordinates = schoolData.location.coordinates;
                query.push({ "location": { $geoWithin: { $center: [coordinates, +searchOptions.distance * 0.0009817477042468395] } } });
            }
            else if (searchOptions.lng && searchOptions.lat && searchOptions.distance) {
                const coordinates = [+searchOptions.lng, +searchOptions.lat];
                query.push({ "location": { $geoWithin: { $center: [coordinates, +searchOptions.distance * 0.0009817477042468395] } } });
            }
            if (searchOptions.rating) {
                query.push({ rating: searchOptions.rating });
            }
            if (searchOptions.ageRange) {
                query.push({ statutoryHighAge: { $gte: +searchOptions.ageRange } });
            }
            if (searchOptions.gender) {
                query.push({ "gender.label": searchOptions.gender });
            }
            if (searchOptions.religiousCharacter) {
                query.push({ "religiousCharacter.label": searchOptions.religiousCharacter });
            }
            if (query.length) {
                MongoDb.edubaseSchool.find({
                    $and: query
                }, { projection: projectionFieldForSchoolSearch.projection }).toArray(function (error, documents) {
                    if (error)
                        reject(error);
                    resolve(documents);
                });
            }
            else {
                resolve(schoolData);
            }
        }).catch((error) => {
            reject(error);
        });
    });
};

const fetchSchool = (options) => {
    if (options.urn && options.distance) {
        return dbConnectors.fetch(MongoDb.edubaseSchool, { uniqueReferenceNumber: +options.urn });
    }
    else {
        return Promise.resolve(undefined);
    }
};

const projectionFieldForSchoolSearch = {
    projection: {
        prefEmail: "",
        TelephoneNum: 1,
        HeadTitle: "",
        HeadFirstName: "",
        HeadLastName: "",
        establishmentName: 1,
        uniqueReferenceNumber: 1,
        location: 1,
        statutoryHighAge: 1,
        "gender.label": 1,
        religiousCharacter: 1,
        rating: 1,
        postcode: 1,
        images: "",
        ofstedLastInsp: "",
        "typeOfEstablishment.label": "",
        "LSOA.label": "",
        statutoryLowAge: 1
    }
};
