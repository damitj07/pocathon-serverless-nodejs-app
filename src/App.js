const serverless = require('serverless-http');
const bodyParser = require('body-parser');
const express = require('express')
const app = express();
const Joi = require("joi");

const responseHandler = require('./utils/responseHandler');
const joiValidator = require('./utils/joiValidationMiddleWare');
const jwt = require('jsonwebtoken');
const subModules = require("./sub_modules");

const MongoDb = require("./configure/db.configure").MongoDb;
const connectDatabase = require("./configure/db.configure").connectDatabase;
const isDatabaseConnected = require("./configure/db.configure").isDatabaseConnected;
const JWT_SECRET_KEY = process.env.JWT_SECRET_KEY || 'config.auth.secret'

app.use(bodyParser.json({ strict: false }));
app.use((req, res, next) => {
    isDatabaseConnected(MongoDb)
        .then((isConnected) => {
            return isConnected ? Promise.resolve(true) : connectDatabase();
        }).then(() => {
            next();
        }).catch((err) => {
            console.log("Error connecting database!!!", err);
            next('Error connecting database!!!');
        });
});

// ./ Db connection
app.get('/', function(req, res) {
    isDatabaseConnected(MongoDb).then((isConnected) => {
        res.send({ data: "Welcom to Pocathon!!!", isConnected: isConnected });
    })
});

// #Users SubModule Routes
app.use("/user", subModules.User);

// #Schools SubModule Routes
app.use("/school", subModules.School);

// #School Search Module Routes
app.use("/searchSchools", subModules.Search);
app.get('/temp/users/:userId', joiValidator.validateParams({ userId: Joi.string().required() }), function(req, res) {
    return MongoDb.users
        .findOne({ 'userId': req.params.userId })
        .then((user) => {
            res.send(responseHandler.responseSuccess(200, "User fetch Success", { user: user }));
        }).catch(err => {
            res.send(responseHandler.responseFail(500, JSON.stringify(err)));
        })
})

// Create User endpoint
const saveUserSchema = { userId: Joi.string().required(), name: Joi.string().required() };
app.post('/temp/users', joiValidator.validateBody(saveUserSchema), function(req, res) {
    MongoDb.users.insertOne(req.body)
        .then((savedUser) => {
            console.log("user::: ", savedUser.ops);
            res.send(responseHandler.responseSuccess(200, "User Save Success", { user: savedUser.ops }));
        }).catch(err => {
            console.log("err== ", err);
            res.send(responseHandler.responseFail(500, JSON.stringify(err)));
        })
});


// JWT scenario

app.get('/getJWT', function(req, res) {
    res.send(responseHandler.responseSuccess(
        200,
        "Token access Success", {
            token: createToken({
                userId: "parimal",
                name: "parimal yeole"
            })
        }
    ));
});

app.post('/getJWT', function(req, res) {
    res.send(responseHandler.responseSuccess(
        200,
        "Token access Success", {
            token: createToken({
                userId: "parimal",
                name: "parimal yeole"
            })
        }
    ));
});

function createToken(user) {
    return jwt.sign(user, JWT_SECRET_KEY, { expiresIn: 60 * 60 * 5 });
}


module.exports.appLocal = app;
module.exports.handler = serverless(app);