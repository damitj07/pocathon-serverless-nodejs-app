const jwt = require('jsonwebtoken');

exports.jwtAuthVerify = (event, context, callback) => {
  const token = event.authorizationToken;
  const split = token.split(' ');
  const policy = {
    principalId: 'userId',
    policyDocument: {
      Version: '2012-10-17',
      Statement: [
        {
          Action: 'execute-api:Invoke',
          Effect: null,
          Resource: event.methodArn,
        },
      ],
    },
    context,
  };

  let effect;
  if (split.length === 2 && split[0] === 'Bearer') {
    jwt.verify(split[1], 'config.auth.secret', (err, deocdeToken) => {
      if (err) {
        console.log("err:: ", err);
        effect = 'Deny';
      } else {
        effect = 'Allow';
      }
      policy.policyDocument.Statement[0].Effect = effect;
      callback(null, policy);
    });
  } else {
    policy.policyDocument.Statement[0].Effect = 'Deny';
    callback(null, policy);
  }
};