module.exports.fetch = (col, fetchOption) => {
    return new Promise((res, rej) => {
        col.findOne(fetchOption, (error, data) => {
            if (error) rej(error);
            res(data);
        });
    });
};

module.exports.insert = (col, userObj) => {
    return new Promise((res, rej) => {
        col.insert(userObj, (error, user) => {
            if (error) rej(error);
            res(user);
        });
    });
};

module.exports.fetchByQuery = (col, term) => {
    return new Promise((res, rej) => {
        if (term.projection) {
            col.findOne(term.query, { projection: term.projection }, (error, result) => {
                if (error) rej(error);
                res(result);
            });
        } else {
            col.findOne(term.query, (error, result) => {
                if (error) rej(error);
                res(result);
            });
        }
    });
};
