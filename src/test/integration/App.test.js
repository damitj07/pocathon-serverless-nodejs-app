const assert = require('assert');
const chai = require('chai');
const chaiHttp = require('chai-http');
var token = "";
chai.use(chaiHttp);
var appUrl = "https://ceatw5kb30.execute-api.us-east-1.amazonaws.com/dev"

describe("Serverless Pocathon testcases", () => {
    before((done) => {
        /**
         * The token will work for all envoirnmets . 
         * Do not change the api url .
         */
        chai.request(appUrl)
        .get('/getJWT')
        .end((err, res) => {
            assert.equal(res.status, 200);
            assert(res.body.data.token);
                token = "Bearer " + res.body.data.token;
                done();
            });
    });
    
    it('\t it should post user', (done) => {
        chai.request(appUrl)
            .post('/temp/users')
            .set('content-type', 'application/json')
            .send({ "userId": "id1", "name": "user1" })
            .end((err, res) => {
                if (err) {
                    console.log("Error occurred in test case", err);
                }
                assert.equal(res.status, 200);
                assert(res.body.data.user.length);
                assert.equal(res.body.data.user[0].userId, "id1");
                assert.equal(res.body.data.user[0].name, "user1");
                done();
            });
    });

    it('\t it should get user data', (done) => {
        chai.request(appUrl)
        .get(`/temp/users/id1`)
        .set('cache-control', 'no-cache')
        .end((err, res) => {
            if (err) {
                console.log("Error occurred while getting data", err);
            }
            assert.equal(res.status, 200);
            assert.equal(res.body.data.user.userId, "id1");
            assert.equal(res.body.data.user.name, "user1");
            done();
        });
    });
});