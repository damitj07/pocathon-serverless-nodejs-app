const moment = require('moment');

module.exports.responseSuccess = function (statusCode, msg, data) {
    return {
        success: true,
        statusCode: statusCode,
        message: msg,
        data: data,
        timestamp: moment.now()
    };
};

module.exports.responseFail = function (statusCode, msg) {
    return {
        success: false,
        statusCode: statusCode,
        message: msg,
        data: undefined,
        timestamp: moment.now()
    }
}