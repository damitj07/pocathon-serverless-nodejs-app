
const Joi = require("joi");
const opts = { abortEarly: false }

module.exports.validateBody = (schema) => {
    return (req, res, next) => {
        validate(req.body, res, next, schema);
    }
}

module.exports.validateQuery = (schema) => {
    return (req, res, next) => {
        validate(req, res, next, schema);
    }
}

module.exports.validateParams = (schema) => {
    return (req, res, next) => {
        validate(req.params, res, next, schema);
    }
}

const validate = (contextToValidate, res, next, schema) => {
    Joi.validate(contextToValidate, schema, opts, err => {
        if (!err) return next()
        res.status(400).send(err)
    })
}

