const MongoClient = require('mongodb').MongoClient;
const mongoUrl = process.env.MONGO_URI || 'mongodb://localhost:27017';

const MongoDb = {
    dbInstance: null,
    users: null,
    edubaseSchool: null,
    statistics: null
};

module.exports.MongoDb = MongoDb;
module.exports.connectDatabase = function () {
    return MongoClient.connect(mongoUrl)
        .then((db) => {
            console.log("Database connected!!!");
            const dbV1 = db.db("nikDb");
            MongoDb.dbInstance = dbV1;
            MongoDb.users = dbV1.collection("users");
            MongoDb.edubaseSchool = dbV1.collection("edubaseSchoolNew");
            MongoDb.statistics = dbV1.collection("statisticsNew");
            return true;
        });
}

module.exports.isDatabaseConnected = function (MongoDb) {
    return MongoDb && MongoDb.dbInstance && MongoDb.dbInstance.serverConfig.isConnected() ? Promise.resolve(true) : Promise.resolve(false);
}
